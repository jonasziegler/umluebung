package at.zie.baumschule;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private String name;
	private int size;
	private List<Tree> treeList;
	
	public Area(String name, int size) {
		super();
		this.name = name;
		this.size = size;
		this.treeList = new ArrayList<Tree>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public void fertilizeAll() {
		for (Tree tree : treeList) {
			tree.doFertilize();
		}
	}
	public void addTree(Tree treeName) {
		treeList.add(treeName);
	}
	
}

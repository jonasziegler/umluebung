package at.zie.baumschule;

public class Tree implements FertilizeMethode{
	private int maxSize;
	private int maxDiameter;
	private FertilizeMethode useMethode;

	
	public Tree(int maxSize, int maxDiameter, FertilizeMethode useMethode) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.useMethode = useMethode;
		
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

	@Override
	public void doFertilize() {
		// TODO Auto-generated method stub
		useMethode.doFertilize();
	}
	
	
}

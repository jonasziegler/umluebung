package at.zie.baumschule;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Producer p1 = new Producer("Jonas");
		Area a1 = new Area("Area51", 50);
		Tree t1 = new LeafTree(25, 7, new SuperGrow());
		Tree t2 = new Conifer(30, 8, new TopGrow());
		
		p1.addArea(a1);
		a1.addTree(t1);
		a1.addTree(t2);
		
		a1.fertilizeAll();
	}

}

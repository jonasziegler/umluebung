package at.zie.baumschule;

import java.util.ArrayList;
import java.util.List;

public class Producer {
	private String name;
	private List<Area> areaList;
	
	public Producer(String name) {
		super();
		this.name = name;
		this.areaList = new ArrayList<Area>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void addArea(Area areaName) {
		areaList.add(areaName);
	}
}
